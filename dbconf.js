#!/usr/bin/node
'use strict';

let dbconf_lib;
//    = new (require('./lib/dbconf-lib'))(__dirname + '/config.json' );
const chalk                = require('chalk');
const figlet               = require('figlet');
const ESC                  = '\x1b[';
const pkg                  = require('./package');
const program              = require('./lib/commander_async');
const { print }            = require('./modules/print_methods');
const { print_object }     = require('./modules/print_methods');

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);
const readFile = promisify(fs.readFile);

const url = require('url');
const path = require('path');
const isArray     = function(a){return (!!a) && (a.constructor === Array);};
const isObject    = function(a){return (!!a) && (a.constructor === Object)};


/*
process.argv.push('fix');
process.argv.push('conf');
process.argv.push('test');

process.argv.push('op');
process.argv.push('validate');
process.argv.push('dt');
process.argv.push('Агент=Осипов');
*/

//process.argv.push('help');

program
	.name(pkg.name)
	.version(pkg.version)
	.description( 'Интерфейс коммандной строки для упревления подключением и структурой БД. \
Простая утилита для подготовки базы данных к работе. Может быть использована для оперативной \
проверки подключения к базе данных, а так же проверки и приведении в эталонному состоянию структуры БД..');

//                                                                ==========   TOOLS  ===========
program
	.command('sync')
	.description('Автомитический выбор шаблона и соединения')
	.option( '-t', '--skip-temp-sync', 'Пропустить автовыбор шаблона', null, 'false' )
	.option( '-с', '--skip-conn-sync', 'Пропустить автовыбор соединения', null, 'false' )
	.action(async function(cmd, argv) {
		
		( '-c' in cmd.options ) || ( await sync_with_local_connection() ) ; // Если не указан -c выбрать соединение автоматически
		( '-t' in cmd.options ) || ( await sync_with_local_template() ) ;   // Если не указан -t выбрать шаблон автоматически
	});


//                                                                ===========   DB  =============

program
	.command('reconn')
	.alias('reconnect')
	.alias('reload')
	.description('Переподключение к базе данных')
	.action(async function() {
		await reload();
	});

program
	.command('set')
	.description('Задать параметры для подключения')
	.long('<confnum>')
	.note('Параметр задаётся только для элемента спика подключений. Чтобы это поключение вступило в силу, необходимо сменить текущее подключение на этот элемент списка строк подключения командой cc (cd).')
	.action(async function(cmd, argv) {
		argv.confnum = parseInt(cmd.values['confnum']);
		argv.conf = dbconf_lib.conf();
		if( argv.confnum>(argv.conf.list.length-1) ) throw new Error('Выберите номер строки подключения. Значение в пределах от 0 до ' + (argv.conf.list.length-1));
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
		}
	});

program
	.command('set')
	.command('user')
	.long('<username>')
	.description('Указать имя пользователя')
	.note('Смена пользователя для подключения')
	.action(async function(cmd, argv) {
		argv.username = cmd.values['username'] || '';
		print('Смена пользователя на ' + argv.username + ' ... ', 0);
		if( !argv.username ) throw new Error('Имя пользователя не может быть пустым');
		const cs = argv.conf.list[ argv.confnum ];
		const parsed = url.parse( cs );
		let a = (parsed.auth || '').split(/\:/);
		a[0] = argv.username;
		parsed.auth = a.join(':');
		argv.conf.list[ argv.confnum ] = url.format( parsed );
		print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
		await save();
	})




program
	.command('set')
	.command('pass')
	.long('<password>')
	.description('Указать пароль пользователя')
	.note('Смена пароля для подключения')
	.action(async function(cmd, argv) {
		argv.password = cmd.values['password'] || '';
		print('Смена пороля ... ', 0);
		const cs = argv.conf.list[ argv.confnum ];
		const parsed = url.parse( cs );
		let a = (parsed.auth || '').split(/\:/);
		if( !a[0] ) throw new Error('Вначале должен быть указан пользователь');
		a[1] = argv.password;
		parsed.auth = a.join(':');
		argv.conf.list[ argv.confnum ] = url.format( parsed );
		print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
		await save();
	})

program
	.command('scan')
	.description('Поиск строк указанных в конфигурационных файлах (json)')
	.action(async function(cmd, argv) {
		argv.list = await find_conn();
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
			if( argv.list.length>0){
				print('Найдены строки:');
				for( let i in argv.list ){
					let cs = argv.list[i];
					print( i + ') ' + conn_toString(cs) );
				}
			}else{
				print('Строки подключения не найдены');
			}
		}
	});

program
	.command('scan')
	.command('add')
	.long('<position>')
	.description('Просканировать и добавить')
	.note('position - номер строки в списке найденных строк подключения, которая будет запомнена или ключевое слово "all" для запоминания всех найденных строк')
	.action(async function(cmd, argv) {
		let foradd;
		if(!cmd.values['position']) throw new Error('Пропущено указание позиции. См. help scan add');
		if(cmd.values['position']=='all'){
			foradd = argv.list;
		}else{
			const position = parseInt(cmd.values['position']);
			if( position => 0){
				if( position<=(argv.list.length-1) ){
					foradd = [ argv.list[position] ];
				}else{
					throw new Error('Позиция отсутствует');
				}
			}else{
				throw new Error('Не корректная позиция');
			}
		}
		if(foradd.length>0){
			let conf = dbconf_lib.conf();
			if( !conf.list ) conf.list = [];
			while( foradd.length>0 ){
				const cs1_str = foradd.pop();
				if( isConfExist( cs1_str, conf.list)==-1 ){
					print('Добавление строки ' + conn_toString( cs1_str ) + ' в список сохранённых подключений ...', 0);
					conf.list.push( cs1_str );
					print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
					await save();
				}
			}
		}
	});



program
	.command('temp')
	.long('<filename>')
	.description('Работа с файлом описания базы')
	.action(async function(cmd, argv) {
		if(cmd.values['filename']) argv.filename = cmd.values['filename'].toString().replace(/^ +/).replace(/ +$/);
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
			const conf = dbconf_lib.conf();
			if( argv.filename ){
				if( !conf.db ) conf.db={};
				const jsonfullname = path.resolve(process.cwd(), argv.filename);
				print('Установка конфигурации базы: ' + jsonfullname + ' ... ', 0);
				await stat(jsonfullname);
				conf.db.structure_file = jsonfullname;
				print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
				await save();
			}else{
				print(conf.db.structure_file);
			}
		}
	});

program
	.command('ls')
	.alias('list')
	.description('Вывод списка сохранённых подключений')
	.option( '-c', '--cmdmode', 'Выводить в формате командной строки для подключения при помощи pgsql', null, 'false' )
	.action(async function(cmd, argv) {
		const conf = dbconf_lib.conf();
		const cmdmode = !!('-c' in cmd.options);
		for(const i in conf.list){
			const parsedUrl = url.parse( conf.list[i] );
			print( i + ') ' + ((i==conf.select)?'* ':'  ') + conn_toString( conf.list[i], cmdmode ));
		}
	});


/*
	.description('Вывод короткой справки по командам CLI')
	.action( async function(cmd){
		const color = !('-n' in cmd.options);

*/


program
	.command('cc')
	.description('Выбор конфигурации из списка (Change Connection)')
	.alias('cd')
	.long('<confnum>')
	.action(async function(cmd) {
		const confnum = parseInt(cmd.values['confnum']);
		await cc( confnum );
	});


program
	.command('rm')
	.alias('del')
	.description('Удаление строки из списка запомненных строк подключения')
	.long('<confnum>')
	.action(async function(cmd) {
		const confnum = parseInt(cmd.values['confnum']);
		const conf = dbconf_lib.conf();
		if( confnum>(conf.list.length-1) ) throw new Error('Выберите номер строки подключения. Значение в пределах от 0 до ' + (conf.list.length-1));
		let cs = conf.list[confnum];
		conf.list.splice(confnum, 1);
		print( 'Строка ' +  cs + ' удалена' );
		delete conf.select;
		await save();
	});


program
	.command('test')
	.description('Общий тест')
	.action(async function(cmd, argv) {
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
			const result = await dbconf_lib.db_test();
			print('\n   Общий результат: ' + (result.result?(ESC + '32mok' + ESC + '0m'):(ESC + '31mНе готова' + ESC + '0m')))
			print('');
		}
	});

program
	.command('test')
	.command('temp')
	.description('Тест структуры')
	.action(async function() {
		const result = await dbconf_lib.db_test_conf({stdout: process.stdout});
		print('');
	});

program
	.command('test')
	.command('conn')
	.description('Тест соединения с БД')
	.action(async function() {
		const result = await dbconf_lib.db_test_conn({stdout: process.stdout});
		print('');
	});

program
	.command('save')
	.long('<filename> <schema>')
	.description('Сохранить конфигурацию БД в файл-эталон')
	.action(async function(cmd, argv) {
		if(cmd.values['filename']) argv.filename = cmd.values['filename'].toString().replace(/^ +/).replace(/ +$/);
		argv.schema = (cmd.values['schema'])?cmd.values['schema'].toString().replace(/^ +/).replace(/ +$/):'';
		const conf = dbconf_lib.conf();
		if( argv.filename ){
			if( !conf.db ) conf.db={};
			const jsonfullname = path.resolve(process.cwd(), argv.filename);
			print('Установка конфигурации базы: ' + jsonfullname + ' ... ', 0);
			conf.db.structure_file = jsonfullname;
			print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
			await save();
		}
		if(conf.db.structure_file){;
			let schema = argv.schema || '';
			if( !schema ){
				schema = 'public';
				let src = require( conf.db.structure_file );
				if( src || src.schema || src.schema.name ) schema = src.schema.name;
			}
			if(schema){
				print('Сохранение схемы "' + schema + '" в файл ' + conf.db.structure_file + ' ... ', 0);
				const result = await dbconf_lib.db_conf_save({schema});
				print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
			}else{
				throw new Error('Не удалось определить схему. Она должна быть указана либо в файле конфигруации (' + conf.db.structure_file + ') либо указана явно в команде save');
			}
		}else{
			throw new Error('Не указан файл структуры БД. Укажите имя файла для сохранения: conf save <filename>.json ');
		}
	});

program
	.command('fix')
	.alias('diff')
	.option('-f', '--force', 'Вносить измененияв базу. Без этой опции, предполагаемые изменения БД лишь отображаются в консоли')
	.option('-c', '--cmdmode', 'Вывод SQL-команд в простом виде, пригодном для отправки в psql или файл, для последующего исполнения средой PostgreSQL')
	.description('Синхронизировать структуру БД с файлом-эталоном')
	.action(async function(cmd, argv) {
		if( '-f' in cmd.options ) argv.force = true;
		if( '-c' in cmd.options ) argv.cmdmode = true;
		const result = await dbconf_lib.db_conf_fix(argv);
	});


//                                                                =========== OTHER =============
program
	.command('help')
	.alias('man')
	.long( '[<commandname>] [<commandname2>] [<commandname3>] [<commandname4>] [<commandname5>]' )
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-f', '--full', 'Полный help по всем командам', null, 'false' )
	.option( '-m', '--md', 'Выводить используя разметку md', null, 'false' )
	.description('Вывод короткой справки по командам CLI')
	.action( async function(cmd){
		const color = !('-n' in cmd.options);
		const full = !!('-f' in cmd.options);
		const md = !!('-m' in cmd.options);
		const style = md?2:(color?1:0);
		if( cmd.values && Object.keys(cmd.values).length>0 ){
			const commandnames =  Object.keys(cmd.values).sort().map(c=>{ return cmd.values[c];}).filter(c => {return c;});
			await program.help_of_commands( commandnames, style );
		}else{
			await program.help_main( style );
			if( full ){
				await program.help_full( style );
			}
		}
	});

program
	.command('exit')
	.alias('quit')
	.alias('выход')
	.description('Выход из интерфейса командной строчки')
	.action(async function() {
		program.stop();
		print('Bye Bye..');
		process.exit(0);
	});

program
	.command('do')
	.long('<commandfilename>')
	.description('Выполненить командный файл')
	.note('Команды из файла выполняются одна за одной. Строки, первый значимый (не пробельный) символ которых является знак "#" (решётка) пропускаются.\n\nТаким образом для коментирования текста командного файла можно использовать этот символ (решётка).')
	.action(async function(cmd) {
		//const commandfilename = cmd.values['commandfilename'];
		//await dbconf_lib.do( commandfilename, program, async function( line ){
		//	await program.parse( program.splitCommand( line ) );
		//});
		const commandfilename = cmd.values['commandfilename'];
		await program.doit( commandfilename, async function( line ){
			await program.parse( program.splitCommand( line ) );
		});
	});

//                                                                -------------------------------

//                                                                =========== START =============
program.start(
	async () =>{
		//onstart
		//print('');
		dbconf_lib = await reload( true );
	},
	async () =>{
		//onstop
		await dbconf_lib.stop();
	},
	async (err) =>{
		//onerror
		print( ESC + '31m' +  'ERR: ' + ESC + '0m' +  err.message );
		//await dbconf_lib.stop();
	},
	async () =>{
		//title
		print('');
		print( chalk.green(figlet.textSync(program.getName().toUpperCase(), { horizontalLayout: 'full' })) );
		print( '\n' + 'Версия: ' + program.getVersion() );
		print( '\n' + 'Используй команду help, если не знаешь, что делать дальше \n' );
	}
);
//                                                                -------------------------------

async function reload( silent ){
	silent || print('Переподключение ', 0);
	let confpath;
	if( dbconf_lib ){
		confpath = dbconf_lib.confpath()
		await dbconf_lib.stop();
	}else{
		confpath = __dirname + '/config.json'
	}
	dbconf_lib = new (require('./lib/dbconf-lib'))(confpath);
	silent || print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
	return dbconf_lib;
}

async function find_temp(){
	const apath = process.cwd().split(/\//).filter( d=>{return d});
	const templist = [];
	await find(process.cwd(), async function( fname ){
		if( fname.match(/\.json$/) ){
			try{
				const jsonfile = eval( '('  + (await readFile( fname )) +')' );
				if( ('list' in jsonfile) && ('roles' in jsonfile) && ('schema' in jsonfile) && ('extentions' in jsonfile) ){
					templist.push( fname );
				}
			}catch(e){}
		}
	});
	return templist;
}

async function find(path, callbackFunc, ilist){
	const list = ilist || [];
	let apath = path.split(/\//).filter( d=>{return d});
	const dirs = await readdir( path );
	for( let el of dirs){
		let f = path + '/' + el;
		let stats = await stat( f );
		if( stats.isDirectory() ){
			await find( f, callbackFunc, list);
		}else{
			await callbackFunc( f );
		}
	}
	return list;
}

async function find_conn(){
	const apath = process.cwd().split(/\//).filter( d=>{return d});
	const list = [];
	while(apath.length>0){
		const dirs = (await readdir('/' + apath.join('/'))).filter( d => {return d.match(/\.json$/)});
		for(const fname of dirs){
			const filename = '/' + apath.join('/') + '/' + fname;
			try{
				const file = require(filename);
				scan_file_for_connstring(file, list);
			}catch(e){}
			//list.push();
		};
		apath.pop();
	}
	return list;
}

function scan_file_for_connstring(obj, list){
	for(const k in obj){
		if( (k == 'connectionstring') && (typeof(obj[k])=='string') ) list.push( obj[k] );
		if( isObject( obj[k] ) ){
			scan_file_for_connstring(obj[k], list);
		}else if( isArray( obj[k] ) ){
			for( let a of obj[k] ){
				if( isObject( a ) ){
					scan_file_for_connstring(a, list);
				}
			}
		}
	}
}

function conn_toString( strUrl, cmdmode ){
	const parsedUrl = url.parse( strUrl );
	const username = ( parsedUrl.auth?(parsedUrl.auth.split(/\:/)[0] + '@'):'');
	if( cmdmode ){
		return 'psql -U ' + username.replace(/\@$/, '') + ' -h ' +  parsedUrl.hostname + ' -p ' + (parsedUrl.port || '5432') + ' ' + parsedUrl.path.replace(/^\//, '')
	}else{
		return parsedUrl.protocol + '//' + username + parsedUrl.hostname + ':' + (parsedUrl.port || '5432') + parsedUrl.path
	}
}

function isConfExist( cs1_str, list ){
	const cs1_parsed = url.parse( cs1_str );
	const username = ( cs1_parsed.auth?(cs1_parsed.auth.split(/\:/)[0] + '@'):'');
	const {protocol} = cs1_parsed;
	const {hostname} = cs1_parsed;
	const port = (cs1_parsed.port || '5432');
	const {path} = cs1_parsed;
	let index = -1;
	for( let i in list ){
		const str = list[i];
		const cs2_parsed = url.parse( str );
		if(
			(cs2_parsed.protocol == protocol)
			&&
			(cs2_parsed.hostname == hostname)
			&&
			((cs2_parsed.port || '5432') == port)
			&&
			(cs2_parsed.path == path)
			&&
			(( cs2_parsed.auth?(cs2_parsed.auth.split(/\:/)[0] + '@'):'') == username)
		) index = i;
	}
	return index;
}

// Смена текущего подключения
async function cc( confnum ){
	const conf = dbconf_lib.conf();
	if( confnum>(conf.list.length-1) ) throw new Error('Выберите номер строки подключения. Значение в пределах от 0 до ' + (conf.list.length-1));
	conf.select = confnum;
	const connectionstring = conf.list[confnum];
	if( !conf.db ) conf.db = {};
	conf.db.connectionstring = connectionstring;
	await dbconf_lib.stop();
	await reload();
	const result = await dbconf_lib.db_test_conn({stdout: process.stdout});
	print('');
	print('Cохранение конфигурации ('  + dbconf_lib.confpath() + ')  ... ', 0);
	await dbconf_lib.conf_save();
	print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');

}

// sync command actions: 
async function sync_with_local_template(){
	const conf = dbconf_lib.conf();
	const templist = await find_temp();
	if(templist.length >0 ){
		if(templist.length==1){
			if( !conf.db ) conf.db={};
			print('Установка конфигурации базы: ' + templist[0] + ' ... ', 0);
			conf.db.structure_file = templist[0];
			print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
			await save();
		}else{
			throw new Error('Several templates was found. Use manual settings.');
		}
	}else{
		throw new Error('Template not found');
	}
}

// sync command actions: 
async function sync_with_local_connection(){
	const conf = dbconf_lib.conf();
	const list = await find_conn();
	if( list.length>0 ){
		if( list.length==1 ){
			let num = isConfExist( list[0], conf.list );
			if( num != -1 ){
				await cc( num, conf );
			}else{
				throw new Error('Can\'t define connection');
			}
		}else{
			throw new Error('Ambigous connection list');
		}
	}else{
		throw new Error('Connection string not found');
	}
}

async function save(){
	print('Cохранение конфигурации ('  + dbconf_lib.confpath() + ') ... ', 0);
	await dbconf_lib.conf_save();
	print('[ ' + ESC + '32m' + 'ok' + ESC + '0m' +' ]');
}
