'use strict';
/**
 *  Общее Тестирование базы данных
 */

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const sql_db_struct = require('../modules/sql_db_struct');
const {getStructure_file} = require('../modules/utils');
const { print }     = require('../modules/print_methods');
const ESC = '\x1b[';


function ifanydiffexist(diff){
	let s = 0;
	for( let tabname of Object.keys(diff) ){
		s = s + (diff[tabname].diff || []).length;
	}
	return (s>0);
}

module.exports = function (pdb, aslib, conf) {
	const db_test_conn = (require('./db_test_conn'))(pdb, aslib, conf);
	const db_test_env = (require('./db_test_env'))(pdb, aslib, conf);
	const db_conf_info = (require('./db_conf_info'))(pdb, aslib, conf);
	return async function ({force=false, cmdmode=false}, idata) {
		const data = idata || {};
		let r1 = await db_test_conn({});
		if(r1.result){
			let r2 = await db_test_env({});
			if(r2.result){
				const dst = eval( '(' +await readFile(getStructure_file( conf )) +')' );
				if( (!dst.schema) || (!dst.schema.name) ) throw new Error('Database schema not defined');
				dst.index = {};
				dst.list.forEach( rel =>{ dst.index[ rel.name ] = rel });
				const src = await sql_db_struct.get( pdb, ( (dst.schema?dst.schema.name:'') || 'public') );
				data.diff = await sql_db_struct.diff(src, dst);
				try{
					if( ifanydiffexist( data.diff ) ){
						await pdb.sql( 'BEGIN;', []);
						await doStep(pdb, data, force, cmdmode, 1);
						await doStep(pdb, data, force, cmdmode, 2);
						await pdb.sql( 'COMMIT;', []);
						if(force){
							print('\n   Все изменения успешно применены');
						}else{
							if(!cmdmode) print('\n   Используйте параметр -f для применения перечисленных изменений к базе. Будьте осторожны!');
						}
					}else{
						print('\n   База соответствует эталону');
					}
				}catch(e){
					await pdb.sql( 'ROLLBACK;', []);
					print('\n   Изменения были отменены (' + e.message + ')');
				}
				if(!cmdmode) print('');
				return data;
			}else{
				throw new Error('Common test failed. Aborted.');
			}
		}else{
			throw new Error('Connection test failed. Aborted.');
		}
		return data;
	}
};

async function doStep(pdb, data, force, cmdmode, step){
	for( let tabname in data.diff ){
		let list = (step==2?(data.diff[tabname].diff2 || []):(data.diff[tabname].diff || []));
		if( list.length>0 ){
			if(!cmdmode){
				if(tabname=='-'){
					print( '\n   Общие ' + ( (step==2)?'(шаг 2)':'' ) + '\n' );
				}else{
					print( '\n   Таблица ' + ESC + '4m' + tabname + ' ' + ( (step==2)?'(шаг 2)':'' ) + ESC + '0m\n' );
				}
			}
			for( let sqlcmd of list ){
				try{
					if(!cmdmode){
						print( '      ' + ESC + '32m' + ' - ' + ESC + '0m [ ' + ESC + '2m' + sqlcmd + ESC + '0m' + ' ]', 0 );
					}else{
						print( sqlcmd, 0);
					}
					if(force){
						print(' ... ', 0 );
						await pdb.sql( sqlcmd, [] );
						if(!cmdmode){
							print( '[ ' + ESC + '32m' + 'ok' + ESC + '0m' + ' ] ' );
						}else{
							print( '[ ok ]' );
						}
					}else{
						print('');
					}
				}catch(err){
					if(!cmdmode){
						print( '[ ' + ESC + '31m' + 'err' + ESC + '0m' + ' ] ( ' + err.message + ' )' );
					}else{
						print( '[ err ] ( ' + err.message + ' )' );
					}
					throw new Error('Failed');
				}
			}
		}
	}
}
