'use strict';
/**
 *  Общее Тестирование базы данных
 */

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const sql_db_struct = require('../modules/sql_db_struct');

const {getStructure_file} = require('../modules/utils');

module.exports = function (pdb, aslib, conf) {
	const db_test_conn = (require('./db_test_conn'))(pdb, aslib, conf);
	const db_test_env = (require('./db_test_env'))(pdb, aslib, conf);
	const db_conf_info = (require('./db_conf_info'))(pdb, aslib, conf);
	return async function ({schema='public'}, idata) {
		const data = idata || {};
		let r1 = await db_test_conn({});
		if( r1.result ){
			const r = await sql_db_struct.get( pdb, schema );
			let structure_file_path = getStructure_file( conf );
			await writeFile( structure_file_path, JSON.stringify({list:r.list, schema: r.schema, roles: r.roles, extentions: r.extentions}, null, 4), 'UTF-8' );
		}else{
			throw new Error('Connection test failed. Aborted.');
		}
		return data;
	}
};

