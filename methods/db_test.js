'use strict';
/**
 *  Общее Тестирование базы данных
 */

module.exports = function (pdb, aslib, conf) {
	const db_test_conn = (require('./db_test_conn'))(pdb, aslib, conf);
	const db_test_conf = (require('./db_test_conf'))(pdb, aslib, conf);
	const db_test_env = (require('./db_test_env'))(pdb, aslib, conf);
	return async function (iarg, idata) {
		const data = idata || {};
		data.tests = data.tests || [];
		await db_test_env({ stdout: process.stdout }, data );
		await db_test_conn( { stdout: process.stdout }, data );
		await db_test_conf( { stdout: process.stdout }, data );

		data.result = data.tests.reduce( (prev, t)=>{
			return !prev?false:t.result;
		}, true);

		return data;
	}
};
