'use strict';
/**
 *  Конфигурация. Сохранение.
 */
const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);

module.exports = function (pdb, aslib, conf, confpath) {
	return async function () {
		await writeFile( confpath, JSON.stringify(conf, null, 4), 'utf8');
		return true;
	};
};
