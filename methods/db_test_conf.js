'use strict';
/**
 *  Тестирование подключения к базе данных
 */

const fs = require('fs');
const { promisify } = require('util');
const stat = promisify(fs.stat);
const path = require('path');
const {getStructure_file} = require('../modules/utils');
const sql_db_struct = require('../modules/sql_db_struct');

const { print_test_item_start }= require('../modules/print_methods');
const { print_test_item_result }= require('../modules/print_methods');

module.exports = function (pdb, aslib, conf) {
	const db_conf_info = (require('./db_conf_info'))(pdb, aslib, conf);
	return async function ({ stdout=null }, idata) {
		const data = idata || {};
		data.tests = data.tests || [];

		let test
		let diff;
		let dst;

		// Тест файла структуры
		test = { subj: 'Тест структуры файла описания базы ', result: false, message: 'Тестирование не проводилось', group:'Общий тест структуры БД', level:0 };
		data.tests.push(test);
		if( stdout ) print_test_item_start(test, stdout);
		try{
			let structure_file = getStructure_file( conf );
			dst = require( structure_file );
			dst.index = {};
			dst.list.forEach( rel =>{ dst.index[ rel.name ] = rel });
			test.result = true;
			test.message = 'ok';
		}catch(e){
			test.result = false;
			test.message = e.message;
		}
		if( stdout ) print_test_item_result(test, stdout);

		if(test.result){
			const schemaname =  (dst.schema?dst.schema.name:'');
			if( !schemaname ) throw new Error('Schema not defined');

			let src = await sql_db_struct.get( pdb,  schemaname  );
			diff = await sql_db_struct.diff(src, dst);

			// Тест таблиц
			for(let tabname in dst.index){
				if(dst.index[tabname].type=='tab'){
					test = { subj: 'Проверка таблицы ' + tabname, result: false, message: 'Тестирование не проводилось', group:'Тест структуры таблиц', level:0 };
					data.tests.push(test);
					if( stdout ) print_test_item_start(test, stdout);
					try{
						await pdb.sql('select count(*) as c from ' + tabname, []);
						if(diff[tabname] &&  diff[tabname].diff && diff[tabname].diff.length>0 ) throw new Error('Обнаружена разница в стркутуре БД и эталоном');
						test.result = true;
						test.message = 'ok';
					}catch(e){
						test.result = false;
						test.message = e.message;
					}
					if( stdout ) print_test_item_result(test, stdout);
				}
			}

			// Тест последовательностей
			for(let tabname in dst.index){
				if(dst.index[tabname].type=='seq'){
					test = { subj: 'Проверка посдедовательности ' + tabname, result: false, message: 'Тестирование не проводилось', group:'Тест последовательностей', level:0 };
					data.tests.push(test);
					if( stdout ) print_test_item_start(test, stdout);
					try{
						if(diff[tabname] &&  diff[tabname].diff && diff[tabname].diff.length>0 ) throw new Error('Обнаружена разница в стркутуре БД и эталоном');
						test.result = true;
						test.message = 'ok';
					}catch(e){
						test.result = false;
						test.message = e.message;
					}
					if( stdout ) print_test_item_result(test, stdout);
				}
			}

			// Общий тест структуры
			test = { subj: 'Тест не описанных отношений ', result: false, message: 'Тестирование не проводилось', group:'Общий тест структуры БД', level:0 };
			data.tests.push(test);
			if( stdout ) print_test_item_start(test, stdout);
			try{
				if(diff['-'] &&  diff['-'].diff && diff['-'].diff.length>0 ){
					throw new Error('Обнаружены общие отклонения базы от эталона (личние отношения, различия в правах на схему, расширения и т.п.)');
				}
				test.result = true;
				test.message = 'ok';
			}catch(e){
				test.result = false;
				test.message = e.message;
			}
			if( stdout ) print_test_item_result(test, stdout);
		}
		data.result = data.tests.reduce( (prev, t)=>{
			return !prev?false:t.result;
		}, true);

		return data;
	};
};
