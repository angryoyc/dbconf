'use strict';
/**
 *  Возвращает сведения о строке подключения к базе данных
 */

//const { mergeInto } = require('cf');

module.exports = function (pdb) {
	return async function () {
		return await pdb.info();
	};
};
