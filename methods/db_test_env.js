'use strict';
/**
 *  Тестирование подключения к базе данных
 */
const fs = require('fs');
const { promisify } = require('util');
const stat = promisify(fs.stat);
const path = require('path');

const { print_test_item_start }= require('../modules/print_methods');
const { print_test_item_result }= require('../modules/print_methods');

module.exports = function (pdb, aslib, conf) {
	return async function ({ stdout=null }, idata) {
		const data = idata || {};
		data.tests = data.tests || [];
		const group = 'Система';
		let stats;

		// Тестирование наличия пути к утилите  pg_dump
		let pg_dump_path = '/usr/bin/pg_dump';
		if(conf.sys && conf.sys['pg_dump']) pg_dump_path = conf.sys['pg_dump'];
		let test = { subj: 'Путь к утилите pg_dump "' + pg_dump_path + '"', result: false, message: 'Тестирование не проводилось', group, level:1 };
		data.tests.push(test);
		if( stdout ) print_test_item_start(test, stdout);
		try{
			stats = await stat(pg_dump_path);
			if( !stats.isFile() ) throw new Error('pg_dump - tool not found');
			test.result = true;
			test.message = 'ok';
		}catch(e){
			test.result = false;
			test.message = e.message;
		}
		if( stdout ) print_test_item_result(test, stdout);

		// Тестирование наличия в конифигурации указания на файл со структурой БД
		test = { subj: 'Путь к файлу с конфигурацией БД', result: false, message: 'Тестирование не проводилось', group, level:1 };
		data.tests.push(test);
		if( stdout ) print_test_item_start(test, stdout);
		try{
			if(!('db' in conf)) throw new Error('Config part "db" is not found');
			if(!('structure_file' in conf.db)) throw new Error('Config part "db/structure_file" is not found');
			let structure_file = (conf.db && conf.db.structure_file)?conf.db.structure_file:'';
			test.subj = 'Путь к файлу с конфигурацией БД - "' + structure_file + '"';
			structure_file = structure_file.replace(/^ +/,'').replace(/ +$/,'');
			if(structure_file){
				if(structure_file.match(/^\./)){
					structure_file = path.normalize(process.cwd() +  '/' + structure_file.replace(/^\./,''));
				}else if(!structure_file.match(/^\//)){
					structure_file = path.normalize(__dirname +  '/../' + structure_file);
				}
			}else throw new Error('Config part "db/structure_file" is empty');
			stats = await stat(structure_file);
			if(!stats.isFile()) throw new Error('Path is not file');
			test.result = true;
			test.message = 'ok';
		}catch(e){
			test.result = false;
			test.message = e.message;
		}
		if( stdout ) print_test_item_result(test, stdout);

		data.result = data.tests.reduce( (prev, t)=>{
			return !prev?false:t.result;
		}, true);

		return data;
	};
};
