'use strict';
/**
 *  Конфигурация БД. Список таблиц
 */

const sql_db_struct = require('../modules/sql_db_struct');

module.exports = function (pdb, aslib, conf) {
	return async function (iarg, idata) {
		const data = idata || {};
		const r = await sql_db_struct.get( pdb, 'bu' );
		data.list = r.list
		.filter( t=>{
			return (t.type == 'tab');
		})
		.map( t=>{
			return t.name;
		});
		return data;
	}
};
