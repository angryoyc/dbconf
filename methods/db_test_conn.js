'use strict';
/**
 *  Тестирование подключения к базе данных
 */

const url = require('url');
const { print_test_item_start }= require('../modules/print_methods');
const { print_test_item_result }= require('../modules/print_methods');

module.exports = function (pdb, aslib, conf) {
	return async function ( { stdout=null }, idata) {
		let data = idata || {};
		data.tests = data.tests || [];
		let group='Тест подключения';

		// Тест наличия строки подключения
		let test = { subj: 'Наличие строки подключения в конфигурации', result: false, message: 'Тестирование не проводилось', group, level:0 };
		data.tests.push(test);
		if( stdout ) print_test_item_start(test, stdout);
		try{
			if( !conf.db ) throw new Error('Part "db" is not found in config');
			if( !conf.db.connectionstring ) throw new Error('Part "db.connectionstring" is not found in config');
			test.result = true;
			test.message = 'ok';
		}catch(e){
			test.result = false;
			test.message = e.message;
		}
		if( stdout ) print_test_item_result(test, stdout);

		// Тест синтаксиса строки подключения
		test = { subj: 'Формальная проверка строки подключения', result: false, message: 'Тестирование не проводилось', group, level:0 };
		data.tests.push(test);
		if( stdout ) print_test_item_start(test, stdout);
		try{
			const parsedUrl = url.parse(conf.db.connectionstring);
			if(parsedUrl.protocol!='postgres:') throw new Error('Bad connection string');
			if( !parsedUrl.hostname ) throw new Error('Empty hostname');
			if( !(parseInt(parsedUrl.port || '5432')>0) ) throw new Error('Invalid port');
			if( !(parsedUrl.path.replace(/^\//,'')) ) throw new Error('Empty DB-name');
			test.result = true;
			test.message = 'ok';
		}catch(e){
			test.result = false;
			test.message = e.message;
		}
		if( stdout ) print_test_item_result(test, stdout);

		// Тестовое подключение к базе данных
		test = { subj: 'Подключение и тестовый запрос', result: false, message: 'Тестирование не проводилось', group, level:0 };
		data.tests.push(test);
		if( stdout ) print_test_item_start(test, stdout);
		try{
			const result = await pdb.sql('select 123 as c;', []);
			if( !(result.rows.length > 0) ) throw new Error('No returning data on SQL-request');
			if( !(result.rows[0].c == 123) ) throw new Error('Bad data in response');
			test.result = true;
			test.message = 'ok';
		}catch(e){
			test.result = false;
			test.message = e.message;
		}
		if( stdout ) print_test_item_result(test, stdout);

		data.result = data.tests.reduce( (prev, t)=>{
			return !prev?false:t.result;
		}, true);

		return data;
	};
};
