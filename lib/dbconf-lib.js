'use strict';

const require_tree = require('require-tree');
const { print } = require('../modules/print_methods');
const url = require('url');

let confpath;
let conf;
let pdb;

module.exports = function(iConfpath, aslib){
	confpath = iConfpath || '../config.json'
	this['confpath'] = function(){ return confpath; }
	conf = require(confpath);
	this['conf'] = function(){ return conf; }
	if( conf.db && conf.db.connectionstring ){
		const parsedUrl = url.parse( conf.db.connectionstring || '' );
		//print('(использую соединение "' + parsedUrl.protocol + '//' + parsedUrl.hostname + ':' + parsedUrl.port + parsedUrl.path + '") ', 0);
		pdb = new (require('../modules/pdb'))(conf.db);
	}else{
		print('(WARNING: DB configuration not found) ', 0);
		//throw 'DB configuration not found';
	}
	let methods = require_tree('../methods');
	for(let m in methods){
		if( pdb ){
			this[m] = (methods[m])(pdb, aslib || false, conf, confpath);
		}else{
			if( m == 'stop' ){
				this[m] = function(){return {};};
			}else{
				this[m] = function(){
					//print('Please select connection string!');
					throw new Error('Please select connection string!');
					return {};
				}
			}
		}
	}
};

