'use strict';

const isArray       = function(a){return (!!a) && (a.constructor === Array);};

//let structs = {};

exports.get = async function(pdb, schemname){
	const data = {
		list: [],
		index: {},
		roles:{}
	};

	// получим схему
	const r0 =  await pdb.sql('SELECT pg_catalog.pg_namespace.nspname as schema_name FROM pg_catalog.pg_namespace where pg_catalog.pg_namespace.nspname=$1;', [schemname] );
	if(r0.rows.length>0){
		data.schema = { name: schemname };
	}

	// список установленных расширений
	const r8 =  await pdb.sql( 'select * from pg_available_extensions where name!=\'plpgsql\' and installed_version is not null;', [] );
	data.extentions = r8.rows.map( r => {
		return r.name;
	});

	// Получаем список отношений (таблиц и последовательностей) в схему с их owner
	const r1 = await pdb.sql('select n.nspname as schema, c.relname as name, u.usename as owner, c.relkind as relkind from pg_class c join pg_namespace n on n.oid = c.relnamespace left join pg_user u on u.usesysid = c.relowner where (c.relkind = \'S\' or c.relkind = \'r\') and n.nspname=$1;', [ schemname ]);
	for( let r of r1.rows ){
		const rel = {
			type: (r.relkind=='S')?'seq':((r.relkind=='r')?'tab':''),
			name: r.schema + '.' + r.name,
			owner: r.owner
		};
		data.index[ rel.name ] = rel;
		data.list.push( rel );
	}

	// Получим все GRANTы для схемы и пропишем их для всех отношений схемы
	const r2 = await pdb.sql('select relname as name, coalesce(nullif(s[1], \'\'), \'public\') as grantee, s[2] as privileges from  pg_class c join pg_namespace n on n.oid = relnamespace  join pg_roles r on r.oid = relowner, unnest(coalesce(relacl::text[], format(\'{%s=arwdDxt/%s}\', rolname, rolname)::text[])) acl, regexp_split_to_array(acl, \'=|/\') s where nspname = $1;', [ schemname ] );
	for( let r of r2.rows ){
		if(schemname + '.' + r.name in data.index){
			let rel = data.index[ schemname + '.' + r.name ];
			if( !rel.grants ) rel.grants = {};
			let roles;
			if( r.grantee!=rel.owner ){
				rel.grants[ r.grantee ] = r.privileges;
				data.roles[ r.grantee ] = rolesAddGrant("U", data.roles[ r.grantee ]);
			}
			data.roles[ rel.owner ] = rolesAddGrant("O", data.roles[ rel.owner ]);;
		}
	}

	// Пропишем поля для таблиц схемы
	const r3 = await pdb.sql('SELECT table_name, column_name, ordinal_position, column_default, is_nullable, data_type, character_maximum_length, columns.udt_name FROM information_schema.columns where table_schema=$1 order by table_name, ordinal_position;', [ schemname ]);
	for( let r of r3.rows ){
		if(schemname + '.' + r.table_name in data.index){
			let rel = data.index[ schemname + '.' + r.table_name ];
			if( !rel.fields ) rel.fields = {};
			rel.fields[r.column_name]={
				'name': r.column_name,
				'ord': r.ordinal_position,
				'default': r.column_default,
				'notnull': (r.is_nullable=='NO')?true:false,
				'type': r.data_type,
				'maxlen': r.character_maximum_length
			};
			if(rel.fields[r.column_name].type.match(/^character varying/)) rel.fields[r.column_name].type = 'varchar';
			if(rel.fields[r.column_name].type.match(/^USER-DEFINED/)) rel.fields[r.column_name].type = r.udt_name;
			let m = (rel.fields[r.column_name].default || '').match(/\'(.+)\'\:\:(character varying|text)/) //';
			if(m) rel.fields[r.column_name].default = m[1];
		}
	}

	// Ограничения
	// Список ограничений схемы
	const r4 = await pdb.sql('SELECT tc.table_schema as "schema", tc.constraint_name as constraint, tc.table_name as table, tc.constraint_type as "type" FROM information_schema.table_constraints AS tc  WHERE tc.table_schema=$1 and (tc.constraint_type=\'FOREIGN KEY\' or tc.constraint_type=\'PRIMARY KEY\')', [ schemname ]);
	for( let r of r4.rows ){
		if( schemname + '.' + r.table in data.index){
			const rel = data.index[ schemname + '.' + r.table ];
			if( !rel.constraints ) rel.constraints = {};
			if( !(r.constraint in rel.constraints) ) rel.constraints[ r.constraint ] = {'type':(r.type.match(/^(.+) KEY$/))[1], fields:[]};
			const c = rel.constraints[ r.constraint ];
			if(c.type=='FOREIGN'){
				c.ffields = c.ffields || [];
			}
		}
	}

	const r5 = await pdb.sql('select cl.relname as table, c.conname as constraint, pg_get_constraintdef(c.oid) as def from pg_constraint c left join pg_namespace n on c.connamespace=n.oid left join pg_class cl on cl.oid=conrelid where n.nspname=$1 order by 1,2,3;',  [ schemname ]);
	for( let r of r5.rows ){
		if( schemname + '.' + r.table in data.index){
			const rel = data.index[ schemname + '.' + r.table ];
			if( r.constraint in rel.constraints ){
				const c = rel.constraints[ r.constraint ];
				// здесь разбор поля DEF с выниманием полей и удалённой таблицы
				let m = r.def.match(/KEY \((.+?)\)/);
				if( m ){
					let columns = m[1].split(/, {0,1}/);
					while( columns.length>0 ) c.fields.push( columns.shift() );
				}else{
					throw new Error('Column list not found in constraint ' + r.constraint);
				}
				if( c.type=='FOREIGN' ){
					if( !c.ffields ) c.ffields = [];
					m = r.def.match(/REFERENCES (.+)\((.+)\)/);
					if( m ){
						c.ftable = m[1];
						let columns = m[2].split(/, {0,1}/);
						while( columns.length>0 ) c.ffields.push( columns.shift() );
					}else{
						throw new Error('Foreign column list and foreign table not found in constraint ' + r.constraint);
					}
					m = r.def.match(/ON DELETE (CASCADE|NO ACTION)/);
					if( m ){
						c.ondelete = m[1];
					}
					m = r.def.match(/ON UPDATE (CASCADE|NO ACTION)/);
					if( m ){
						c.onupdate = m[1];
					}
				}
			}
		}
	}

	// Пропишем для последовательностей имя связанного поля (OWNED)
	for(let rel of data.list){
		if( rel.type=='tab' ){
			for( let fieldname in rel.fields ){
				let field = rel.fields[ fieldname ];
				let m = (field.default || '').match(/nextval\('(.+)'/);
				if( m ){
					if(!m[1].match(/\./)) m[1] = 'public.' + m[1];
					if( m[1] in data.index ){
						data.index[ m[1] ].owned =  rel.name + '.' + fieldname;
						if( !rel.sequences ) rel.sequences = [];
						rel.sequences.push( m[1] );
					}
				}
			}
		}
	}

	// Index list
	const r7 = await pdb.sql('SELECT tablename, indexname, indexdef FROM pg_indexes WHERE schemaname = $1;', [ schemname ]);
	for( let r of r7.rows ){
		if( (schemname + '.' + r.tablename) in data.index ){
			const rel = data.index[ (schemname + '.' + r.tablename) ];
			if( !(r.indexname in (rel.indexes || {})) ){
				if( !(r.indexname in (rel.constraints || {})) ){
					if( !rel.indexes ) rel.indexes = {};
					if( !rel.indexes[ r.indexname ] ) rel.indexes[ r.indexname ] = {};
					let idx = rel.indexes[ r.indexname ];
					let m = r.indexdef.match(/^CREATE (.*) *INDEX (.+) ON (.+) USING (.+) \((.+)\)/);
					if(m){
						if( m[2] == r.indexname ){
							m[1] = m[1].replace(/ +$/,'');
							idx.unique = ( m[1]=='UNIQUE' );
							idx.using  = m[ 4 ];
							idx.fields = m[ 5 ].split(/, /);
						}
					}
				}
			}
		}
	}


	// права на схему
	const r9 = await pdb.sql( "SELECT * FROM  pg_namespace pn WHERE pn.nspname=$1 ;", [ schemname ] );
	if(r9.rows.length>0){
		if( !data.schema.grants ) data.schema.grants={};
		for( let r of r9.rows ){
			let m = (r.nspacl || '').match(/\{(.+)\}/);
			if(m){
				let a = m[1].split(',');
				for( let rl of a){
					let b = rl.split(/=/);
					let grants = b[1].split(/\//)[0]; //'
					let role = b[0];
					data.schema.grants[role] = grants;
				}
			}
		}
	}
	return data;
};

exports.diff = async function( src, dst ){
	const cmds = {};
	check_schema( cmds, src, dst );
	check_schema_grants( cmds, src.schema, dst.schema );
	//check_extentions( cmds, src, dst );
	for( const src_rel of src.list ){
		if( !(src_rel.name in dst.index)){
			console.log( src_rel.name );
			drop_table_command( cmds, src, dst, src_rel );
		}
	}
	for( const rel of dst.list ) if(rel.type=='tab') makesql(cmds, src, dst, rel);
	return cmds;
};

//проверяем наличие буквы grantlett в строке прав grantstring, если нет, то возвращаем дополненную этой буквой строку, иначе, возвращаем исходную grantstring
function rolesAddGrant( grantlett, grantstring ){
	if( (grantstring || '').split('').indexOf(grantlett)<0){
		return ((grantstring || '') + grantlett);
	}else{
		return (grantstring || '');
	}
}

// проверка наличия схемы
function check_schema( cmds, src, dst ){
	if( (!dst.schema) || (!dst.schema.name) ){
		throw new Error('dst schemname not found');
	}else{
		if( (!src.schema) || (!src.schema.name) || (dst.schema.name!=src.schema.name) ){
			if( !cmds[ '-' ] ) cmds[ '-' ] = { diff:[], name:'-' };
			cmds[ '-' ].diff.push('CREATE SCHEMA ' + quote( dst.schema.name ) + ';');
		}
	}
}

// проверка наличия схемы
function check_schema_grants( cmds, src, dst ){
	if( dst && dst.name ){
		if( !cmds[ '-' ] ) cmds[ '-' ] = { diff:[], name:'-' };
		// для каждого требуемого разрешения проверим его наличие по факту
		for( let role in (dst.grants || {}) ){
			const dst_grant = dst.grants[ role ];
			if( src && ( role in (src.grants || {}) ) ){
				let src_grant = src.grants[ role ];
				create_schema_grant( cmds, dst, role, grant_diff( src_grant, dst_grant ) );
			}else{
				create_schema_grant( cmds, dst, role, dst_grant );
			}
		}
		// для каждого фактического разрешения проверим его наличие в эталоне
		// проверим указан ли фактический релейшен. Если нет, то не проверяем вообще.
		if( src ){
			for( let role in (src.grants || {}) ){
				const src_grant = src.grants[ role ];
				if( role in (dst.grants || {}) ){
					let dst_grant = dst.grants[ role ];
					revoke_schema_grant( cmds, dst, role, grant_diff( dst_grant, src_grant ) );
				}else{
					revoke_schema_grant( cmds, dst, role, 'UC' );
				}
			}
		}
	}else{
		throw new Error('dst schemname not found');
	}
}


// проверка наличия расширений
function check_extentions( cmds, src, dst ){
	if( (dst.extentions || []).length>0 ){
		for( const ext of dst.extentions){
			if((src.extentions || []).indexOf(ext)<0){
				if( !cmds[ '-' ] ) cmds[ '-' ] = { diff:[], name:'-' };
				cmds[ '-' ].diff.push('CREATE EXTENSION IF NOT EXISTS ' + quote( ext ) + ';');
			}
		}
	}
}


function makesql(cmds, src, dst, rel){
	// ТАБЛИЦЫ
	if( rel.name in src.index ){
		// Отношение уже присутствует. Проверим структуру.
		// Проверим список последовательностей
		const src_rel = src.index[ rel.name ];
		if( isArray(rel.sequences) ){
			for( let seqname of rel.sequences ){
				if( !(seqname in dst.index) ) throw new Error('Reference for unknown sequence (tab.' + rel.relname + ')');
				if( !(seqname in src.index) ){
					if( seqname in dst.index ){
						create_seq_command( cmds, dst.index[ seqname ] );
					}else{
						throw new Error('Indvalid data of model');
					}
				}else{
					check_rel_grants(cmds, dst.index[ seqname ], src.index[ seqname ], rel);
				}
			}
		}

		// Проверим список полей
		let fldname;
		for( fldname in rel.fields ){
			const fld = rel.fields[ fldname ];
			if( fldname in src_rel.fields ){
				// поле уже присутствует. Сравним свойства.
				const src_fld = src_rel.fields[ fldname ];
				if( src_fld['default'] != fld['default'] ){
					alter_column_default_command( cmds, rel, fld );
				}
				if( src_fld.notnull != fld.notnull) alter_column_notnull_command( cmds, rel, fld );
				if( (src_fld.type != fld.type) || (src_fld.maxlen != fld.maxlen) ) alter_column_type_command( cmds, rel, fld );
			}else{
				// поле отсутствует. Добавим поле
				add_column_command( cmds, rel, fld );
			}
		}

		// удаляем поля, которых нет в эталонной модели
		for( fldname in src_rel.fields ){
			const fld = src_rel.fields[ fldname ];
			if( !(fldname in rel.fields) ){
				drop_column_command( cmds, src_rel, fld );
			}
		}

		// проверяем разрешения ( GRANT )
		check_rel_grants(cmds, rel, src_rel);

		// проверяем ограничения
		// для каждого ограничения эталона 
		for(let constrname in (rel.constraints || {}) ){
			const constr = rel.constraints[ constrname ];
			// проверяем его наличие в фактической базе
			if( constrname in (src_rel.constraints || {}) ){
				// если есть,
				const src_constr = src_rel.constraints[ constrname ];
				// то проверяем список полей
				if( src_constr.fields.join(',')!=constr.fields.join(',') ){
					// если список полей отличается, то пересоздаём
					drop_constraint_command( cmds, rel, constrname );
					create_constraint_command( cmds, rel, constrname );
				}else{
					// если с полями всё в порядке,
					// то для FOREIGN ограничений 
					if( constr.type=='FOREIGN' ){
						// проверяем списоке полей удалённой таблицы и собственно имя этой удалённой таблицей.
						// и если [список|имятаблицы] отличается 
						if( (src_constr.ffields.join(',')!=constr.ffields.join(',')) || (constr.ftable!=src_constr.ftable) ){
							// то пересоздаём ограничение
							drop_constraint_command( cmds, rel, constrname );
							create_constraint_command( cmds, rel, constrname );
						}
					}
				}
			}else{
				// иначе создаем ограничение constr
				create_constraint_command( cmds, rel, constrname );
			}
		}

		// удалим не указанные в эталоне ограничения
		for(let constrname in (src_rel.constraints || {}) ){
			if( !(constrname in (rel.constraints || {})) ){
				drop_constraint_command( cmds, src_rel, constrname );
			}
		}

		// проверяем индексы
		for( let idxname in (rel.indexes || {}) ){
			const idx = rel.indexes[ idxname ];
			if( idxname in (src_rel.indexes || {}) ){
				const src_idx = src_rel.indexes[ idxname ];
				if( src_idx.fields.join(',')!=idx.fields.join(',') ){
					drop_idx_command( cmds, rel, idxname );
					create_idx_command( cmds, rel, idxname );
				}
			}else{
				create_idx_command( cmds, rel, idxname );
			}
		}

		// удалим не указанные в эталоне индексы
		for(let idxname in (src_rel.indexes || {}) ){
			if( !(idxname in (rel.indexes || {})) ){
				drop_idx_command( cmds, rel, idxname );
			}
		}

	}else{
		// Отношение отсутствует. Сгенерируем команду создания отношения.
		if( rel.type == 'tab' ){
			create_table_command( cmds, src, dst, rel );
			check_rel_grants( cmds, rel );
		}else if( rel.type == 'seq' ){
			create_seq_command( cmds, rel );
			check_rel_grants(cmds, rel );
		}else{
			throw new Error('Unknown relation type');
		}
	}
}

function quote(name){
	if( name.match(/\./) ){
		const a = name.match(/^(.+?)\.(.+)$/);
		return '"' + a[1] + '"."' + a[2] + '"';
	}else{
		return '"' + name + '"';
	}
}

function drop_idx_command( cmds, rel, idxname ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	cmds[ rel.name ].diff.push('DROP INDEX IF EXISTS ' + idxname + ' CASCADE;');
}


function create_idx_command( cmds, rel, idxname ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	const idx = rel.indexes[ idxname ];
	cmds[ rel.name ].diff.push('CREATE ' + ( idx.unique?'UNIQUE ':'' ) + 'INDEX ' + idxname + ' ON ' + quote(rel.name) + ' USING '  + idx.using + ' (' + idx.fields.join(',') + ');');
}


function drop_constraint_command( cmds, rel, constrname ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	cmds[ rel.name ].diff.push('ALTER TABLE ONLY ' + quote(rel.name) + ' DROP CONSTRAINT ' + constrname + ';');
}

function create_constraint_command( cmds, rel, constrname ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	if( !cmds[rel.name].diff2 ) cmds[rel.name].diff2 = [];
	const constr = rel.constraints[ constrname ];
	let cmd = 'ALTER TABLE ONLY ' + quote(rel.name) + ' ADD CONSTRAINT ' + constrname + ' ' + constr.type + ' KEY (' + constr.fields.join(',') + ')';
	if( constr.type=='FOREIGN' ){
		cmd = cmd + ' REFERENCES ' + constr.ftable + '(' + constr.ffields.join(',') + ')';
		if( constr.ondelete ) cmd = cmd + ' ON DELETE ' + constr.ondelete;
		if( constr.onupdate ) cmd = cmd + ' ON UPDATE ' + constr.onupdate;
	}
	cmd = cmd + ';';
	if( constr.type=='FOREIGN' ){
		cmds[ rel.name ].diff2.push(cmd);
	}else{
		cmds[ rel.name ].diff.push(cmd);
	}
}

function check_rel_grants( cmds, rel, src_rel, rel_parent){
	// для каждого требуемого разрешения проверим его наличие по факту
	for( let user in rel.grants ){
		const grant = rel.grants[ user ];
		if( src_rel && ( user in src_rel.grants ) ){
			let src_grant = src_rel.grants[ user ];
			create_rel_grant( cmds, rel, user, grant_diff( src_grant, grant ), (rel_parent?rel_parent.name:rel.name) );
		}else{
			create_rel_grant( cmds, rel, user, grant, (rel_parent?rel_parent.name:rel.name) );
		}
	}
	// для каждого фактического разрешения проверим его наличие в эталоне
	// проверим указан ли фактический релейшен. Если нет, то не проверяем вообще.
	if( src_rel ){
		for( let user in src_rel.grants ){
			const src_grant = src_rel.grants[ user ];
			if( user in rel.grants ){
				let grant = rel.grants[ user ];
				revoke_rel_grant( cmds, rel, user, grant_diff( grant, src_grant ) );
			}else{
				revoke_rel_grant( cmds, rel, user, 'arwdDxt' );
			}
		}
	}
}

function revoke_rel_grant( cmds, rel, user, grant ){
	if( grant=='arwdDxt' ){
		if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
		cmds[ rel.name ].diff.push('REVOKE ALL ON ' + quote(rel.name) + ' FROM "' + user + '";');
	}else{
		for( let g of grant.split('') ){
			if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
			cmds[ rel.name ].diff.push('REVOKE ' + letter2grantname(g) +  ' ON ' + quote(rel.name) + ' FROM "' + user + '";');
		}
	}
}

function create_rel_grant( cmds, rel, user, grant, relname ){
	// GRANT ALL ON TABLE bu.at TO site;
	if( grant=='arwdDxt' ){
		if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
		cmds[ relname || rel.name ].diff.push('GRANT ALL ON ' + ((rel.type=='tab')?'TABLE':'SEQUENCE') + ' '   + quote(rel.name) + ' TO "' + user + '";');
	}else{
		for( let g of grant.split('') ){
			let rn = relname || rel.name;
			if( !cmds[ rn ] ) cmds[rn] = {diff:[], name:rel.name};
			cmds[ rn ].diff.push('GRANT ' + letter2grantname(g) +  ' ON ' + ((rel.type=='tab')?'TABLE':'SEQUENCE') + ' '   + quote(rel.name) + ' TO "' + user + '";');
		}
	}
}

function create_schema_grant( cmds, schema, role, grant ){
	for( let g of grant.split('') ){
		if( !cmds[ '-' ] ) cmds[rn] = {diff:[], name:'-'};
		cmds[ '-' ].diff.push('GRANT ' + letter2grantname(g) +  ' ON SCHEMA '  + quote(schema.name) + ' TO ' + quote(role) + ';');
	}
}

function revoke_schema_grant( cmds, schema, role, grant ){
	for( let g of grant.split('') ){
		if( !cmds[ '-' ] ) cmds['-'] = {diff:[], name:'-'};
		cmds[ '-' ].diff.push('REVOKE ' + letter2grantname(g) +  ' ON SCHEMA ' + quote(schema.name) + ' FROM ' + quote(role) + ';');
	}
}

function grant_diff(src_grant, grant){
	let diff = [];
	for( let lett of grant.split('') ){
		if( src_grant.split('').indexOf(lett)<0 ){
			diff.push(lett);
		}
	}
	return diff.join('');
}

function letter2grantname( lett ){
	switch( lett ) {
	case 'r':
		return 'SELECT';
	case 'w':
		return 'UPDATE';
	case 'a':
		return 'INSERT';
	case 'd':
		return 'DELETE';
	case 'D':
		return 'TRUNCATE';
	case 'x':
		return 'REFERENCES';
	case 't':
		return 'TRIGGER';
	case 'X':
		return 'EXECUTE';
	case 'U':
		return 'USAGE';
	case 'C':
		return 'CREATE';
	case 'c':
		return 'CONNECT';
	case 'T':
		return 'TEMPORARY';
	default:
		return lett;
	}
}

function create_seq_command( cmds, rel ){
	let m = (rel.owned || '').match(/(.+\..+)\.(.+)/);
	let ownedtab = m?m[1]:'';
	let ownedfld = m?m[2]:'';
	if( ownedtab && ownedfld ){
		if( !cmds[ ownedtab ] ) cmds[ ownedtab ] = {diff:[], name:ownedtab};
		cmds[ownedtab].diff.push('CREATE SEQUENCE '  + quote(rel.name) + ' START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;');
		cmds[ownedtab].diff.push('ALTER TABLE '      + quote(rel.name) + ' OWNER TO ' + rel.owner + ';');
		cmds[ownedtab].diff.push('ALTER SEQUENCE '   + quote(rel.name) + ' OWNED BY ' + rel.owned + ';');
		cmds[ownedtab].diff.push('SELECT setval(\''   + rel.name        + '\', max(' + ownedfld + ')) FROM ' + ownedtab + ';');
	}else{
		throw new Error('No OWNED field found in sequence description ' + rel.name);
	}
}

function drop_table_command( cmds, src, dst, rel ){
	if( !cmds[ '-' ] ) cmds[ '-' ] = {diff:[], name:'-'};
	if( rel.type=='tab'){
		cmds[ '-' ].diff.push('DROP TABLE IF EXISTS ' + quote(rel.name) + ' CASCADE;');
	}else{
		cmds[ '-' ].diff.push('DROP SEQUENCE IF EXISTS ' + quote(rel.name) + ' CASCADE;');
	}
}

function create_table_command( cmds, src, dst, rel ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	let flds = [];
	let seqs = [];
	for(const fldname in rel.fields){
		const fld = rel.fields[fldname];
		flds.push( quote(fld.name) + ' '  + fld.type + (fld.maxlen?'('+fld.maxlen+')':'') + (fld.notnull?' NOT NULL':'') );
		let m = (fld.default || '').match(/nextval\('(.+)'/);
		if( m ){
			if(  m[1] in  dst.index ){
				seqs.push( fld );
			}else{
				//print('| Unknown sequence "' + m[1] + '" in default expression |', 0);
				//throw new Error('Unknown sequence "' + m[1] + '" in default expression');
			}
		}else{
			seqs.push( fld );
		}
	}
	cmds[rel.name].diff.push('CREATE TABLE ' + quote(rel.name) + ' (' + flds.join(', ') + ');');
	cmds[rel.name].diff.push('ALTER TABLE ' + quote(rel.name) + ' OWNER TO ' + rel.owner + ';');
	if( isArray(rel.sequences) ){
		for( let seqname of rel.sequences ){
			if( !(seqname in src.index) ){
				if( seqname in dst.index ){
					create_seq_command( cmds, dst.index[ seqname ] );
					check_rel_grants( cmds, dst.index[ seqname ], null, rel );
				}else{
					throw new Error('Indvalid data of model');
				}
			}
		}
	}

	// Создание значений по умолчанию
	while( seqs.length>0 ){
		alter_column_default_command( cmds, rel, seqs.pop() );
	}

	// проверяем разрешения ( GRANT )
	check_rel_grants(cmds, rel);

	// Создание индексов
	for(let idxname in (rel.indexes || {}) ){
		const constr = (rel.indexes || {})[idxname];
		create_idx_command( cmds, rel, idxname );
	}

	// Создание первичных ключей
	for(let constrname in (rel.constraints || {}) ){
		const constr = (rel.constraints || {})[constrname];
		create_constraint_command( cmds, rel, constrname );
	}

}

function drop_column_command( cmds, rel, fld ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	cmds[rel.name].diff.push('ALTER TABLE ONLY ' + quote(rel.name) + ' DROP COLUMN IF EXISTS ' + quote(fld.name) + ' CASCADE;' );
}

function add_column_command( cmds, rel, fld ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	cmds[rel.name].diff.push('ALTER TABLE ONLY ' + quote(rel.name) + ' ADD COLUMN ' + quote(fld.name) + ' ' + fld.type + (fld.maxlen?'('+fld.maxlen+')':'') + ';');
	alter_column_default_command( cmds, rel, fld );
	if(fld.default || (fld.default=='')){
		cmds[rel.name].diff.push('UPDATE ' + quote(rel.name) + ' SET ' + quote(fld.name) + '=\'' + fld.default + '\'::' + fld.type + ';');
	}else{
		if( !fld.notnull ) cmds[rel.name].diff.push('UPDATE ' + quote(rel.name) + ' SET ' + quote(fld.name) + '=null;');
	}
	alter_column_notnull_command( cmds, rel, fld )
}

function alter_column_default_command( cmds, rel, fld ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	if( fld.default==null ){
		cmds[rel.name].diff.push('ALTER TABLE ONLY ' + quote(rel.name) + ' ALTER COLUMN ' + quote(fld.name) + ' DROP DEFAULT;');
	}else{
		let defa;
		if( ( (fld.type=='varchar') || (fld.type=='text') || ( fld.type.match(/character varying/) ) ) && ( !(fld.default || '').match(/\'/) ) ){ //'
			defa = '\'' + (fld.default || '') + '\'';
		}else{
			defa = fld.default;
		}
		cmds[rel.name].diff.push('ALTER TABLE ONLY ' + quote(rel.name) + ' ALTER COLUMN ' + quote(fld.name) + ' SET DEFAULT ' + defa + ';');
	}
}

function alter_column_notnull_command( cmds, rel, fld ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	cmds[rel.name].diff.push('ALTER TABLE ONLY ' + quote(rel.name) + ' ALTER COLUMN ' + quote(fld.name) + ' ' + (fld.notnull?'SET NOT NULL':'DROP NOT NULL') + ';');
}

function alter_column_type_command( cmds, rel, fld ){
	if( !cmds[ rel.name ] ) cmds[rel.name] = {diff:[], name:rel.name};
	cmds[rel.name].diff.push('ALTER TABLE ONLY ' + quote(rel.name) + ' ALTER COLUMN ' + quote(fld.name) + ' TYPE ' + fld.type + (fld.maxlen?'('+fld.maxlen+')':'') + ';');
}
