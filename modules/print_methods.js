const ESC                  = '\x1b[';

/**
* v0.2
*/

/**
* Печать строки в STDOUT
* @param {*} s - собственно строка для печати
* @param {boolean} use_cr - признак использования символа "перевода каретки" после строки. Значение по умолчанию: true (каретка переводится)
* @param {stream} stream - поток для вывода. По умолчанию STDOUT
*/

const print = exports.print = function print( s, use_cr, stream ){
	let io = stream || process.stdout;
	let cr =( (typeof(use_cr)=='undefined') || (!!use_cr) )?'\n':'';
	io.write( s + cr );
};

/**
* Печать таблицы
* @param {Object}  header - объект, описывающий колонки - их ширину заголовки, тип данных в них и т.п.
* @param {Object}  rows   - строки для вывода в формате, возвращаемом SQL-запросом (pdb.sql)
* @param {Integer} idots - число, определящие вывод точек (и т.д.) под шапкой или в конце таблицы 0
*                          0 - не выводить; 1 - после заголовка; 2 - в конце; 3 - в обоих случаях;
* @param {stream}  stream - поток для вывода. По умолчанию STDOUT
*/
exports.print_table = function(header, rows, idots, stream){
	let io = stream || process.stdout;
	const dots = idots || 0;
	const N = 20;
	for(let h of header){
		print( '-', 0, io );
		print( '-'.repeat( h.width || N ), 0, io );
		if(header[header.length-1].column!=h.column) print( '-+', 0, io );
	}
	print( '', 1, io );
	for(let h of header){
		print( ' ', 0, io );
		print( printws(h.title, ( h.width || N )), 0, io );
		if(header[header.length-1].column!=h.column) print( ' |', 0, io );
	}
	print( '', 1, io );
	for(let h of header){
		print( '-', 0, io );
		print( '-'.repeat( h.width || N ), 0, io );
		if(header[header.length-1].column!=h.column) print( '-+', 0, io );
	}
	print( '', 1, io );

	if( ((dots & 1)==1) && (rows.length>0) ){
		for(let h of header){
			print( ' ', 0, io );
			print( printws( '...'.substr(0, h.title.length), ( h.width || N )) );
			if(header[header.length-1].column!=h.column) print( ' |', 0);
		}
		print( '', 1, io );
	}

	for( let row of rows ){
		if( row!=null ){
			for(let h of header){
				const a = h.column.split(/\./);
				let r = row;
				for( let i of a ) r=r[i];
				print( ' ', 0, io );
				print( printws( r, ( h.width || N ), h.type), 0, io );
				if(header[header.length-1].column!=h.column) print( ' |', 0, io );
			}
		}else{
			for(let h of header){
				print( '-', 0, io );
				print( '-'.repeat( h.width || N ), 0, io );
				if(header[header.length-1].column!=h.column) print( '-+', 0, io );
			}
		}
		print( '', 1, io );
	}

	if( (dots & 2)==2 ){
		for(let h of header){
			print( ' ', 0, io );
			print( printws( '...'.substr(0, h.title.length), ( h.width || N )), 0, io );
			if(header[header.length-1].column!=h.column) print( ' |', 0, io );
		}
		print( '', 1, io );
	}

	for(let h of header){
		print( '-', 0, io );
		print( '-'.repeat( h.width || N ), 0, io );
		if(header[header.length-1].column!=h.column) print( '-+', 0, io );
	}
	print( '', 1, io );
};

/**
* Преобразование строки с "дотягиванием" до указанной длины
* @param {*} s - собственно строка для печати
* @param {*} len - длина, до которой необходимо дотягивать
* @param {*} type - type=='currency'
*/
const printws = exports.printws = function printws(s, len, type){
	if(type=='currency'){
		if(typeof(s)=='number'){
			const a = s.toString().split(/\./);
			if( a.length<2 ) a.push('00');
			s = a[0] + '.' + (a[1] + '00').substr(0,2);
			s = s + ' р.';
		}
		const n = len - s.length;
		if(n>0) s = ' '.repeat(n) + s;
		return s;
	}else{
		let r = '';
		for(let l = (s || '').toString().length; l<len; l++){ r=r + ' '; }
		return ((s || '').toString() + r);
	}
};

exports.print_stringarray = function ( list, stream ){
	let io = stream || process.stdout;
	for(let i of list) print( i, 1, io );
};

exports.print_object = function ( obj, stream ){
	let io = stream || process.stdout;
	print( JSON.stringify( obj, null, 4), 1, io );
};

/* 
* Печать результатов тестирования
* 
* Входящий массив:
[
	{
		subj: 'Название и содержание теста',
		result: true                                  - результат тестирования
		message: 'Описание ошибки, если result=false или пусто или ok если result=true'
		group: 'Текстовое обозначение группы тестирования, например "Тестирование структуры"'
	},
	...
]
*/

let group;

exports.print_test_item_result = function( tests_itm, stream ){
	let io = stream || process.stdout;
	if(tests_itm.result){
		// ESC + '32m' + ' ✔ ' + ESC + '0m' +
		exports.print( ' [' + ESC + '32m' + ' ✔ ' + ESC + '0m' + '] ' + ((tests_itm.message && (tests_itm.message!='ok'))?' (' + tests_itm.message + ')':'' ), true, io );
	}else{
		// ESC + '31m' + ' - ' + ESC + '0m' +
		exports.print( ' [' + ESC + '31m' + 'err' + ESC + '0m' + '] ( ' + tests_itm.message + ' )', true, io );
	}
}

exports.print_test_item_start = function( tests_itm, stream ){
	let io = stream || process.stdout;
	if( tests_itm.group!=group ){
		group =  tests_itm.group;
		print('\n   ' + ESC + '4m' + group + ESC + '0m', true, io );
	}
	exports.print('      - ' + ESC + '2m' + tests_itm.subj + ' ... ' + ESC + '0m ', false, io );
}

exports.print_test_results = function(tests_arr, stream){
	let io = stream || process.stdout;
	for(let r of tests_arr){
		exports.print_test_item_start( r, io );
		exports.print_test_item_result( r, io );
	}
/*
	let rslt = {};
	let groups = [];
	tests_arr.forEach(t=>{
		if( !( t.group in rslt ) ){
			groups.push(t.group);
			rslt[ t.group ] = { tests:[] };
		}
		rslt[ t.group ].tests.push(t);
	});
	for( const group of groups ){
		let res = rslt[ group ];
		for(let r of res.tests){
			exports.print_test_item_start( r, stream );
			exports.print_test_item_result( r, stream );
		}
	}
*/
}
