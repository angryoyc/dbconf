'use strict';
const path = require('path');
const url = require('url');
const spawn = require('child_process').spawn;

/*
exports.clearcommands = function( text ){
	return text
		.replace(/^ +/,'')       // Удаляем ведущие пробелы
		.replace(/^(\r*\n)+/,'') // Удаляем ведущие возвраты каретки
		.replace(/(\r*\n)+;/,'') // Удаляем возвраты каретки в конце последовательности
		.replace(/ +$/,'')       // Удаляем пробелы в концу
		.replace(/\r\n/g,'\n')   // Меняем виндовые возвраты каретки на линусовые
		.split(/\n/)             // Режем на строки
		.map(l=>{
			return (l.replace(/\t+/g, ' ').replace(/^\s+/, '').replace(/\s+$/, '')); // Очищаем строки от лишних пробелов
		})
		.filter(l=>{
			return l && (!l.match(/^#/));           // исключаем пустые строки и строки-коментарии
		})
	;
};
*/

/* основываясь на данных конфигурации возвращает путь директории с файлами структур таблиц */
exports.getStructure_file = function( conf ){
	let structure_file = (conf.db && conf.db.structure_file)?conf.db.structure_file:'';
	structure_file = structure_file.replace(/^ +/, '').replace(/ +$/, '');
	if(structure_file){
		if(structure_file.match(/^\./)){
			structure_file = path.normalize(__dirname +  '/..' + structure_file.replace(/^\./, ''));
		}else if(!structure_file.match(/^\//)){
			structure_file = path.normalize(__dirname +  '/../' + structure_file);
		}
	};
	return structure_file;
}

exports.readSQLbody = async function (conf, tabname){
	const parsedUrl = url.parse( conf.db.connectionstring );
	let pg_dump_path = '/usr/bin/pg_dump';
	if(conf.sys && conf.sys['pg_dump']) pg_dump_path = conf.sys['pg_dump'];
	let output = await exec(pg_dump_path, ['--schema-only', '-h', parsedUrl.hostname, '-p', parsedUrl.port, '-t', tabname, parsedUrl.path.replace(/^\//,'')  ]);
	const body = output.filter( l=>{
		return !l.match(/^\-\-/);
	}).join('\n');
	return body;
}

async function exec( binname, params){
	return new Promise(( resolve, reject )=>{
		const bin = spawn( binname, params );
		const stderr_data = [];
		const stdout_data = [];
		bin.stdout.on('data', ( data ) => { stdout_data.push(data.toString('utf8')); });
		bin.stderr.on('data', ( data ) => { stderr_data.push(data.toString('utf8')); });
		bin.on('close', ( code ) => {
			let a;
			if( code ){
				//error
				a = stderr_data.join('').split(/\r*\n/);
				if(a.length>0){
					reject(new Error(a[0]));
				}else{
					reject(new Error('Неизвестная ошибка'));
				}
			}else{
				//ok
				a = stdout_data
				.join('')
				.split(/\r*\n/)
				.filter( s=>{return s} );
				resolve( a );
			}
		});
	});
}
