#!./!applay_sql_2_db.sh


-- список индексов (с полями внешних таблиц)

-- SELECT indexname, indexdef FROM pg_indexes where indexname like 'sc2sc%';

SELECT ic.oid,pg_get_indexdef(ic.oid),ic.relname AS name, am.amname, i.indisprimary AS pri,
i.indisunique AS uni, i.indkey AS fields, i.indclass AS fopclass,
i.indisclustered, ic.oid AS indid, c.oid AS relid, ds.description,
u.usename, pg_get_expr(i.indexprs, i.indrelid) AS expr,
ts.spcname, pg_get_expr(i.indpred, i.indrelid) AS wh,
cn.oid IS NOT NULL AS iscn, cn.oid as constroid
FROM pg_index i INNER JOIN pg_class c ON i.indrelid = c.oid
INNER JOIN pg_class ic ON i.indexrelid = ic.oid
INNER JOIN pg_am am ON ic.relam = am.oid
LEFT OUTER JOIN pg_description ds ON ds.objoid = ic.oid
LEFT OUTER JOIN pg_user u ON u.usesysid = ic.relowner
LEFT OUTER JOIN pg_constraint cn ON i.indrelid = cn.conrelid AND ic.relname = cn.conname
LEFT OUTER JOIN pg_tablespace ts ON ts.oid = ic.reltablespace
WHERE
c.oid = ('bu.sc2sc')::regclass::oid
ORDER BY ic.relname