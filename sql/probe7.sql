#!./!applay_sql_2_db.sh


-- -- список GRANTов для отношения
-- SELECT subq.relname, subq.obj_description, subq.attname, d.description
-- FROM
-- (SELECT c.relname, obj_description(c.oid) obj_description, a.attname, c.oid, a.attnum
-- FROM pg_class c, pg_attribute a
-- WHERE c.oid = a.attrelid
-- AND c.relname = 'at2cl'
-- AND a.attnum > 0) subq LEFT OUTER JOIN pg_description d ON (d.objsubid = subq.attnum AND d.objoid = subq.oid)
-- ;

SELECT c.relname, obj_description(c.oid) obj_description, a.attname, c.oid, a.attnum
FROM pg_class c, pg_attribute a
WHERE c.oid = a.attrelid
AND c.relname = 'at2cl'
