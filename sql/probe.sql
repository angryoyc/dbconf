#!./!applay_sql_2_db.sh


-- список ограничений (без полей внешней таблицы)

SELECT
  tc.TABLE_SCHEMA SCHEMA_NAME,
  tc.TABLE_NAME,
  kcu.COLUMN_NAME,
  tc.CONSTRAINT_NAME,
  tc.CONSTRAINT_TYPE
FROM
  information_schema.TABLE_CONSTRAINTS tc
  JOIN information_schema.KEY_COLUMN_USAGE kcu
    ON tc.TABLE_SCHEMA = kcu.TABLE_SCHEMA
    AND tc.TABLE_NAME = kcu.TABLE_NAME
    AND tc.CONSTRAINT_NAME = kcu.CONSTRAINT_NAME
WHERE
  tc.CONSTRAINT_TYPE <> 'FOREIGN KEY'
;


-- список индексов

SELECT
  st.TABLE_SCHEMA,
  st.TABLE_NAME,
  st.COLUMN_NAME,
  st.INDEX_NAME
FROM information_schema.STATISTICS st
WHERE
  st.INDEX_NAME <> 'PRIMARY'
;