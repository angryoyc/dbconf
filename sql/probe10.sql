#!./!applay_sql_2_db.sh


select 
	table_schema as schema, 
	table_name as table, 
	column_name as column, 
	constraint_name as constraint,
	kcu.*
from 
	information_schema.key_column_usage AS kcu 
WHERE
	kcu.constraint_schema='bu'
	and
	table_name='op2sc_dt'
ORDER BY 
	table_name, constraint_name, ordinal_position, position_in_unique_constraint;


select
	table_schema as schema,
	table_name as table,
	column_name as column,
	constraint_name as constraint,
	ccu.position_in_unique_constraint
from
	information_schema.constraint_column_usage AS ccu
WHERE 
	ccu.constraint_schema='bu'
ORDER BY table_schema, table_name, constraint_name
;

SELECT tc.constraint_name,
tc.constraint_type,
tc.table_name,
kcu.column_name,
tc.is_deferrable,
tc.initially_deferred,
rc.match_option AS match_type,

rc.update_rule AS on_update,
rc.delete_rule AS on_delete,
ccu.table_name AS references_table,
ccu.column_name AS references_field
FROM information_schema.table_constraints tc

LEFT JOIN information_schema.key_column_usage kcu
ON tc.constraint_catalog = kcu.constraint_catalog
AND tc.constraint_schema = kcu.constraint_schema
AND tc.constraint_name = kcu.constraint_name

LEFT JOIN information_schema.referential_constraints rc
ON tc.constraint_catalog = rc.constraint_catalog
AND tc.constraint_schema = rc.constraint_schema
AND tc.constraint_name = rc.constraint_name

LEFT JOIN information_schema.constraint_column_usage ccu
ON rc.unique_constraint_catalog = ccu.constraint_catalog
AND rc.unique_constraint_schema = ccu.constraint_schema
AND rc.unique_constraint_name = ccu.constraint_name

WHERE lower(tc.constraint_type) in ('foreign key')
and
	tc.table_name='op2sc_dt'
;

