#!./!applay_sql_2_db.sh


-- список индексов (с полями внешних таблиц)

-- SELECT indexname, indexdef FROM pg_indexes where indexname like 'sc2sc%';


select
    t.relname as table_name,
    i.relname as index_name,
    a.attname as column_name,
    i.*
from
    pg_class t,
    pg_class i,
    pg_index ix,
    pg_attribute a
where
    t.oid = ix.indrelid
    and i.oid = ix.indexrelid
    and a.attrelid = t.oid
    and a.attnum = ANY(ix.indkey)
    and t.relkind = 'r'
--    and t.relname like 'test%'
	and t.relname='sc2sc'
order by
    t.relname,
    i.relname

;
