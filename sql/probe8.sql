#!./!applay_sql_2_db.sh


-- -- список GRANTов для отношения
-- select
--     t.relname as table_name,
--     i.relname as index_name,
--     a.attname as column_name,
--     *
-- from
--     pg_class t,
--     pg_class i,
--     pg_index ix,
--     pg_attribute a
-- where
--     t.oid = ix.indrelid
--     and i.oid = ix.indexrelid
--     and a.attrelid = t.oid
--     and a.attnum = ANY(ix.indkey)
--     and t.relkind = 'r'
-- order by
--     t.relname,
--     i.relname
-- limit 1
-- ;

SELECT tablename, indexname, indexdef FROM pg_indexes WHERE schemaname = 'bu';
-- select t.relname as table_name, i.relname as index_name, a.attname as column_name from pg_class t, pg_class i, pg_index ix, pg_attribute a where t.oid = ix.indrelid and i.oid = ix.indexrelid and a.attrelid = t.oid and a.attnum = ANY(ix.indkey) and t.relkind = 'r' and t.relname='sc2sc' order by t.relname, i.relname;

