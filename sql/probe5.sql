#!./!applay_sql_2_db.sh


-- список таблиц и последовательностей с их owner
select n.nspname as sequence_schema, 
       c.relname as sequence_name,
       u.usename as owner,
       c.relkind as relkind
from pg_class c 
  join pg_namespace n on n.oid = c.relnamespace
  join pg_user u on u.usesysid = c.relowner
where (c.relkind = 'S' or c.relkind = 'r')
and n.nspname='bu'
;

