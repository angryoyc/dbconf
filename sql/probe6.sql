#!./!applay_sql_2_db.sh


-- список GRANTов для отношения

select 
	relname as name,
	coalesce(nullif(s[1], ''), 'public') as grantee,
	s[2] as privileges
from 
	pg_class c
	join pg_namespace n on n.oid = relnamespace
	join pg_roles r on r.oid = relowner,
	unnest(coalesce(relacl::text[], format('{%s=arwdDxt/%s}', rolname, rolname)::text[])) acl, 
	regexp_split_to_array(acl, '=|/') s
where
	"nspname" = 'bu'
	and coalesce(nullif(s[1], ''), 'public')<>'postgres'


;
