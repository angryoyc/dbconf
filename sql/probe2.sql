#!./!applay_sql_2_db.sh


-- список ограничений (с полями внешних таблиц)

SELECT
    tc.table_schema as "schema",
    tc.constraint_name, 
    tc.table_name, 
--    kcu.column_name, 
--    ccu.table_schema AS foreign_schema,
--    ccu.table_name AS foreign_table,
--    ccu.column_name AS foreign_column,
    tc.constraint_type as "type"
FROM 
    information_schema.table_constraints AS tc 
--   INNER JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name  AND tc.table_schema = kcu.table_schema
--   INNER JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = kcu.constraint_name  AND ccu.table_schema = kcu.table_schema
WHERE 
	tc.table_schema='bu'
	and
	(
		tc.constraint_type='FOREIGN KEY'
		or
		tc.constraint_type='PRIMARY KEY'
	)
--	tc.constraint_type = 'FOREIGN KEY'
--	and
--	tc.constraint_name = 'sc2sc_fk2'
--	tc.table_name='sc2sc'
;

select * from information_schema.key_column_usage AS kcu WHERE kcu.table_schema='bu' ORDER BY table_name, column_name, ordinal_position, position_in_unique_constraint;

select * from information_schema.constraint_column_usage AS ccu WHERE ccu.table_schema='bu';